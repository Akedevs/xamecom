﻿using System;
using Newtonsoft.Json;

namespace DemoEcom.Library.Models
{

     
 

    public class Customer
    {
         
            public int CusID { get; set; }
            public int SexID { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string PassWord { get; set; }
            public string Degree { get; set; }
            public string ImagePath { get; set; }
            public string Jobs { get; set; }
            public string Sales { get; set; }
            public string Follower { get; set; }
            public string Products { get; set; }
            public string Bg { get; set; }
            public string Token { get; set; }
            public int UnityType { get; set; }
            public string AssemblyName { get; set; }

        [JsonIgnore]
            public string ImageUrl
            {
                get
                {
                    return $"https://dotnetcores.azurewebsites.net/upload/Customer/{ImagePath}";
                }
            }

    }
}
