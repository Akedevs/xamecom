﻿using System;
namespace DemoEcom.Library.Models
{
    public class Degree
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
