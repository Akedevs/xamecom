﻿using System;
namespace DemoEcom.Library.Models
{
    public class Chart
    {
        public int ObjID { get; set; }
        public string Label { get; set; }
        public int Value { get; set; }
    }
}
