﻿using System;
using Newtonsoft.Json;

namespace DemoEcom.Library.Models
{
    public class Product
    {
        public int productID { get; set; }
        public string VenderName { get; set; }
        public string CategoryName { get; set; }
        public string ProductName { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string ImagePath { get; set; }
        public string Likes { get; set; }
        public string Comments { get; set; }


        [JsonIgnore]
        public string ImageUrl
        {
            get
            {
                return $"https://dotnetcores.azurewebsites.net/upload/Music/{ImagePath}";
            }
        }
    }
}
