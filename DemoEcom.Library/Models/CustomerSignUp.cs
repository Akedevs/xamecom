﻿using System;
namespace DemoEcom.Library.Models
{
    public class CustomerSignUp
    {

            public int SexID { get; set; }
            public string FullName { get; set; }
            public string Email { get; set; }
            public string PassWord { get; set; }
            public string Degree { get; set; }
            public string ImagePath { get; set; }
            public string Agent { get; set; }

    }
}
