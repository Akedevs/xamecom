﻿using System;
using Android.Content;
using DemoEcom.Droid;
using DemoEcom.XamarinRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(RoundedButtonSignOut), typeof(RoundedButtonSignOutRendererAndroid))]
namespace DemoEcom.Droid
{
    public class RoundedButtonSignOutRendererAndroid : ButtonRenderer
    {
        public RoundedButtonSignOutRendererAndroid(Context context) : base(context)
        {
        }


        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {

                Control.Background = Android.App.Application.Context.GetDrawable(Resource.Drawable.rounded_button_signout);
               //Control.SetTextColor(Android.Graphics.Color.ParseColor("#160e00"));

                Control.SetTextColor(Android.Graphics.Color.ParseColor("#FFFFFF"));

            }
        }
    }

}

