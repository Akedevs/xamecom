﻿using System;
using Android.Content;
using Android.Views;
using DemoEcom.Droid;
using DemoEcom.XamarinRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;



[assembly: ExportRenderer(typeof(RoundedEntry), typeof(RoundedEntryRendererAndroid))]
namespace DemoEcom.Droid
{
    public class RoundedEntryRendererAndroid : EntryRenderer
    {
        public RoundedEntryRendererAndroid(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {


                Control.Background = Android.App.Application.Context.GetDrawable(Resource.Drawable.rounded_coner);
                Control.Gravity = GravityFlags.CenterVertical;

                Control.SetTextColor(Android.Graphics.Color.ParseColor("#d8dadd"));
                Control.SetHintTextColor(Android.Graphics.Color.ParseColor("#d8dadd"));

               
            }
        }

    }
}
