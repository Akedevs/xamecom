﻿using System;
using Android.Content;
using Android.Views;
using DemoEcom.Droid;
using DemoEcom.XamarinRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;



[assembly: ExportRenderer(typeof(RoundedEntry), typeof(RoundedEntryRegisterRendererAndroid))]
namespace DemoEcom.Droid
{
    public class RoundedEntryRegisterRendererAndroid : EntryRenderer
    {
        public RoundedEntryRegisterRendererAndroid(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {


                Control.Background = Android.App.Application.Context.GetDrawable(Resource.Drawable.rounded_corner_register);
                Control.Gravity = GravityFlags.CenterVertical;

                Control.SetTextColor(Android.Graphics.Color.ParseColor("#d8dadd"));
                Control.SetHintTextColor(Android.Graphics.Color.ParseColor("#d8dadd"));


            }
        }
    }
}
