﻿using System;
using Android.Content;
using DemoEcom.Droid;
using DemoEcom.XamarinRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;


[assembly: ExportRenderer(typeof(RoundedButton), typeof(RoundedButtonRendererAndroid))]
namespace DemoEcom.Droid
{
    public class RoundedButtonRendererAndroid : ButtonRenderer
    {
        public RoundedButtonRendererAndroid(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null)
            {
               
                Control.Background = Android.App.Application.Context.GetDrawable(Resource.Drawable.rounded_button);   
                Control.SetTextColor(Android.Graphics.Color.ParseColor("#160e00"));

            }
        }
    }
}
