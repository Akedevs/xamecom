﻿using System;
using DemoEcom.Pages;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace DemoEcom
{
    public partial class App : Application
    {
        public static Helpers.Route Route { get; private set; }

        public App()
        {
            InitializeComponent();
            //Dependecies Edit References
            //Project  Tools Edit File
            Route = new Helpers.Route(this);
            Route.StartPage();

            //MainPage = new NavigationPage(new MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
