﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using DemoEcom.Library.Models;
using Newtonsoft.Json;

namespace DemoEcom.Helpers
{
    public class Service
    {

        private static HttpClient GetClient()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://dotnetcores.azurewebsites.net");
            return client;
        }

        private static HttpClient GetClientWithToken()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("https://dotnetcores.azurewebsites.net");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Helpers.Setting.Token);
            return client;
        }

        public static async Task<bool> Login(string email, string password)
        {

            try
            {

                var client = GetClient();
                var response = await client.GetAsync("api/Customer/ecomGetByValue?Email=" + email + "&Password=" + password);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var customers = JsonConvert.DeserializeObject<List<Customer>>(json);
                    if (customers.Count > 0)
                    {
                      // Helpers.Setting.Email = customers[0].Email;
                        Helpers.Setting.FullName = customers[0].FullName;
                        Helpers.Setting.ImageUrl = customers[0].ImageUrl;
                        Helpers.Setting.Token = customers[0].Token;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }



        }

        public static async Task<bool> SignUp(CustomerSignUp customerSignUp)
        {
            var param = new Dictionary<string, string>();
            param.Add("Email", customerSignUp.Email);
            param.Add("Password", customerSignUp.PassWord);
            param.Add("FullName", customerSignUp.FullName);
            param.Add("Degree", customerSignUp.Degree);
            param.Add("Agent", "007");
            param.Add("SexID", customerSignUp.SexID.ToString());
            param.Add("ImagePath", "xamarin.jpg");
            var content = new FormUrlEncodedContent(param);
            try
            {
                var client = GetClient();
                var response = await client.PostAsync("api/Customer/ecomRegister", content);
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var text = await response.Content.ReadAsStringAsync();
                    if (text == "\"1\"")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }



        }

        public static List<string> GetDegrees()
        {
            return new List<string>()
            {
                "Bachelor" , "Master" , "Doctor"
                 //new Degree {Id = 1 , Name = "Bachelor" },
                 //new Degree {Id = 2 , Name = "Master"  },
                 //new Degree {Id = 3 , Name = "Doctor"  }
            };
        }

        public static List<int> GetSexes()
        {
            return new List<int>()
            {
                1,2
                 //new Sex {Id = 1 , Name = "Male"   },
                 //new Sex {Id = 2 , Name = "Female" }
            };
        }

        public static async Task<List<Customer>> GetCustomer()
        {
            try
            {

                var client = GetClientWithToken();
                var response = await client.PostAsync("api/Customer/ecomgetall", null);
                if (response.StatusCode == HttpStatusCode.OK)
                {

                    var json = await response.Content.ReadAsStringAsync();
                    var customers = JsonConvert.DeserializeObject<List<Customer>>(json);
                     

                    if (customers.Count > 0)
                    {
                        return customers;
                    }
                   else
                    {
                        return new List<Customer>();
                    }

                }
                else
                {
                    return new List<Customer>();
                }

            }
            catch (Exception ex)
            { return new List<Customer>(); }


        }

        public static async Task<List<Product>> GetProduct()
        {

            try
            {

                var client = GetClient();
                var response = await client.GetAsync("api/Product/ecomGetAll");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var products = JsonConvert.DeserializeObject<List<Product>>(json);
                    if (products.Count > 0)
                    {
                        return products;

                    }
                    else
                    {
                        return new List<Product>();
                    }
                }
                else
                {
                    return new List<Product>();
                }

            }
            catch (Exception ex)
            {
                return new List<Product>();
            }
        }

        public static async Task<bool> UploadPhoto(string filePath)
        {

            try
            {
                var filebytes = File.ReadAllBytes(filePath);
                MultipartFormDataContent content = new MultipartFormDataContent();
                ByteArrayContent baContent = new ByteArrayContent(filebytes);
                content.Add(baContent, "filePhoto", Guid.NewGuid().ToString() + ".jpg");
                var client = GetClient();
                var response = await client.PostAsync("api/Customer/UploadPhoto", content);
                return response.StatusCode == HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<List<Chart>> GetChart()
        {

            try
            {

                var client = GetClient();
                var response = await client.GetAsync("api/Customer/ecomGetChart");
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var charts = JsonConvert.DeserializeObject<List<Chart>>(json);
                    if (charts.Count > 0)
                    {
                        return charts;

                    }
                    else
                    {
                        return new List<Chart>();
                    }
                }
                else
                {
                    return new List<Chart>();
                }

            }
            catch (Exception ex)
            {
                return new List<Chart>();
            }
        }



    }
}
