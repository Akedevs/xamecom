﻿using System;
using Plugin.Settings;

namespace DemoEcom.Helpers
{
    public class Setting
    {
        // Xam.plugins.settings
        public static string Email
        {
            get { return CrossSettings.Current.GetValueOrDefault("Email", ""); }
            set { CrossSettings.Current.AddOrUpdateValue("Email", value); }
        }

        public static string ImageUrl
        {
            get { return CrossSettings.Current.GetValueOrDefault("ImageUrl", ""); }
            set { CrossSettings.Current.AddOrUpdateValue("ImageUrl", value); }
        }

        public static string FullName
        {
            get { return CrossSettings.Current.GetValueOrDefault("FullName", ""); }
            set { CrossSettings.Current.AddOrUpdateValue("FullName", value); }
        }

        public static string Token
        {
            get { return CrossSettings.Current.GetValueOrDefault("Token", ""); }
            set { CrossSettings.Current.AddOrUpdateValue("Token", value); }
        }
    }
}
