﻿using System;
using System.Threading.Tasks;
using DemoEcom.Pages;
using Xamarin.Forms;

namespace DemoEcom.Helpers
{
    public class Route
    {
        App app;
        NavigationPage nav;
         
        public Page CurrentPage { get; set; }


        public Route(App app)
        {
            this.app = app;
        }

        public void StartPage()
        {
            if (string.IsNullOrEmpty(Helpers.Setting.Email))
            {
                nav = new NavigationPage(new LoginPage());
                app.MainPage = nav;
            }
            else
            {
                // app.MainPage = new MainPage();
                nav = new NavigationPage(new MainPage());
                app.MainPage = nav;
            }
        }


        public void LoggedIn()
        {
            nav = new NavigationPage(new MainPage());
            app.MainPage = nav;
        }


        public void LggedOut()
        {
            nav = new NavigationPage(new LoginPage());
            app.MainPage = nav;
        }


        public async Task PushAsync(Page page)
        {
            // Using Form Tash + Page ========> Alt Enter
            CurrentPage = page;
            await nav.PushAsync(page);
        }

       


        public async Task PopAsync()
        {
            await nav.PopAsync();
        }

         
    }
}
