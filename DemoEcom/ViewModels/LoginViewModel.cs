﻿using System;
using System.Windows.Input;
using DemoEcom.Pages;
using Xamarin.Forms;

namespace DemoEcom.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {


        public string Email { get; set; }
        public string PassWord { get; set; }


        public ICommand SignUpCommand { get; set; }
        public ICommand LoginCommand { get; set; }



        public LoginViewModel()
        {
            Email = "akedevs@gmail.com";
            PassWord = "com";
            SignUpCommand = new Command(SignUpAction);
            LoginCommand = new Command(LoginAction);

        }

        async void SignUpAction(object obj)
        {
            //We Create static at App
             
            await App.Route.PushAsync(new SignUpPage());
        }


        async void LoginAction(object obj)
        {
            //App.Route.LoggedIn();
            IsLoading = true;
            var isOk = await Helpers.Service.Login(Email, PassWord);
            IsLoading = false;
            if (isOk)
            {
                App.Route.LoggedIn();
            }
            else
            {
                Message = "Email or Password Invalid";
            }
        }
    }
}
