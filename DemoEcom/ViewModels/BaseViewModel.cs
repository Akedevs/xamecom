﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace DemoEcom.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged // Opion & Enter 2 Times
    {
         
        public BaseViewModel()
        {

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }


        private bool _isLoading;

        // public event PropertyChangedEventHandler PropertyChanged;

        public bool IsLoading
        {
            get { return _isLoading; }
            set
            {
                _isLoading = value;
                OnPropertyChanged("IsLoading");
            }
        }

        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                _message = value;
                OnPropertyChanged("Message");
            }
        }

        internal Task DisplayAlert(string title, string message, string accept, string cancel)
        {
            throw new NotImplementedException();
        }
    }
}
