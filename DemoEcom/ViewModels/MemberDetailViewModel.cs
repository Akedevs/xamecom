﻿using System;
using DemoEcom.Library.Models;

namespace DemoEcom.ViewModels
{
    public class MemberDetailViewModel : BaseViewModel
    {


        private Customer _customer;
        public Customer Customer
        {
            get { return _customer; }
            set
            {
                _customer = value;
                OnPropertyChanged("Customer");
            }

        }

        public MemberDetailViewModel()
        {
            Customer = new Customer();
        }


        public void LoadCustomer(Customer customer)
        {
            Customer = customer;
            string xxxxx = "akedev";
        }
    }
}
