﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using DemoEcom.Library.Models;
using DemoEcom.Pages;
using Xamarin.Forms;

namespace DemoEcom.ViewModels
{
    public class MemberViewModel : BaseViewModel
    {

        private List<Customer> _customers;

        public List<Customer> Customers
        {

            get { return _customers; }
            set
            {
                _customers = value;
                OnPropertyChanged("Customers");
            }
        }


        public ICommand RefreshCommand { get; set; }
        public ICommand MemberSelectCommand { get; set; }


        private bool _isRefreshing;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged("IsRefreshing");
            }
        }


        public MemberViewModel()
        {

            Customers = new List<Customer>();
            RefreshCommand = new Command(RefreshAction);
            MemberSelectCommand = new Command(MemberSelectAction); // Click at Row
            LoadData();
        }


        async  void MemberSelectAction(object obj)//Obj is Parameter
        {

            var customer = obj as Customer;
            var memberDetailPage = new MemberDetailPage();
            var memberDetailViewModel = memberDetailPage.BindingContext as MemberDetailViewModel;


            memberDetailViewModel.LoadCustomer(customer);
            await App.Route.PushAsync(memberDetailPage);

        }


        async void LoadData()
        {
            IsLoading = true;
            Customers = await Helpers.Service.GetCustomer();
            IsLoading = false;
        }


        async void RefreshAction(object obj)
        {
            Customers = await Helpers.Service.GetCustomer();
            IsRefreshing = false;
        }

         

    }
}
