﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace DemoEcom.Converters
{
    public class BooleanConverter : IValueConverter //Form & Implement Alt & Enter ====> 2 Times
    {
        public BooleanConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //throw new NotImplementedException();
            return !(bool)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //throw new NotImplementedException();
            return !(bool)value;
        }
    }
}
