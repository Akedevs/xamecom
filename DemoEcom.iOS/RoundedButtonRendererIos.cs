﻿using System;
using DemoEcom.iOS;
using DemoEcom.XamarinRenderer;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;



[assembly: ExportRenderer(typeof(RoundedButton), typeof(RoundedButtonRendererIos))]
namespace DemoEcom.iOS
{
    public class RoundedButtonRendererIos : ButtonRenderer
    {
        public RoundedButtonRendererIos()
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
        }
    }
}
